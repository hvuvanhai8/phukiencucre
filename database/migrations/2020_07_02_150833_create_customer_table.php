<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cus_name')->nullable();
            $table->string('cus_email')->nullable();
            $table->string('cus_gender')->nullable();
            $table->string('cus_age')->nullable();
            $table->string('cus_old')->nullable();
            $table->string('cus_phone')->nullable();
            $table->string('cus_address')->nullable();
            $table->string('cus_picture')->nullable();
            $table->string('cus_status')->nullable();
            $table->string('cus_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
