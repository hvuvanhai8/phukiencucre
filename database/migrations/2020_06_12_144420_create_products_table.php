<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pro_code')->nullable();
            $table->string('pro_model')->nullable();
            $table->string('pro_name')->nullable();
            $table->string('pro_slug')->nullable();
            $table->string('pro_quantity')->nullable();
            $table->text('pro_image')->nullable();
            $table->string('pro_origin')->nullable();
            $table->string('pro_capacity')->nullable();
            $table->string('pro_read_random')->nullable();
            $table->string('pro_read')->nullable();
            $table->string('pro_write')->nullable();
            $table->string('pro_brand')->nullable();
            $table->string('pro_guarantee')->nullable();
            $table->string('pro_stability')->nullable();
            $table->string('pro_standard_connect')->nullable();
            $table->string('pro_bus')->nullable();
            $table->string('pro_type')->nullable();
            $table->string('pro_color')->nullable();
            $table->string('pro_sensitive')->nullable();
            $table->string('pro_weight')->nullable();
            $table->string('pro_view')->nullable();
            $table->string('pro_brightness')->nullable();
            $table->string('pro_language')->nullable();
            $table->string('pro_size')->nullable();
            $table->string('pro_cord')->nullable();
            $table->string('pro_switch')->nullable();
            $table->string('pro_battery')->nullable();
            $table->string('pro_wattage')->nullable();
            $table->string('pro_process_speed')->nullable();
            $table->string('pro_cpu_core')->nullable();
            $table->text('pro_teaser')->nullable();
            $table->longText('pro_description')->nullable();
            $table->string('pro_price')->nullable();
            $table->string('pro_price_discount')->nullable();
            $table->string('pro_active')->nullable();
            $table->string('pro_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
