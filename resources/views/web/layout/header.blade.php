
    <header id="home" class="main_header">
        <div class="col_left">
            <div class="bars_mb"><i class="fas fa-bars"></i></div>
            <div class="logo_zilo">
                <a href="/"><img src="/web/images/logo/logo_pk.svg" alt=""></a>
            </div>
        </div>
        <div class="col_right">
            <div class="search_hd">
              <form action="">
                  <input type="text" class="form-control" placeholder="Nhập tên ổ cứng, phụ kiện... cần tìm">
                  <span class="icon_s"><i class="fal fa-search"></i></span>
              </form>
            </div>
            <div class="car_home">
                <a href="/cart" class="btn-login">
                    <i class="fas fa-shopping-cart">
                        <b id="cart_count">{{$count_cart}}</b>
                    </i>
                </a>

            </div>

            <div class="hotline">
                <a href=""><i class="fas fa-phone-alt"></i> 0902.131.474</a>
                <a href=""><i class="fas fa-phone-alt"></i> 0902.131.474</a>
            </div>
        </div>
    </header>

    @include('web.layout.menu')

    <div class="hotline-phone-mobile">
        <div class="ring">
            <div class="ring-circle"></div>
            <div class="ring-circle-fill"></div>
            <div class="ring-img-circle">
                <a href="tel:0902131474" class="btn-img">
                    <img src="/web/images/icon-1.png" width="50">
                </a>
            </div>
        </div>
        <div class="bar">
            <a href="tel:0902131474">
                <span class="c1592410661 text-hotline">0902131474</span> </a>
        </div>
    </div>

    <div id="back-top" class="back-to-top">
        <i class="fas fa-arrow-circle-up"></i>
    </div>

    <div class="navbar_overlap"></div>
