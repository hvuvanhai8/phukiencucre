<footer id="footer">
        <div class="container-fluid main_footer">
            <div class="row">
                <div class="col-md-6">
                    <b class="slogan-ft">Phukiencucre.com - Vượt trên mong đợi</b>
                    <em>“Chúng tôi mang đến cho khách hàng những sản phẩm chất lượng với dịch vụ chu đáo và giá cả tốt nhất.
                        Tất cả các sản phẩm bán ra đều là hàng chính hãng 100%, được bảo hành 1 đổi 1 trong suốt thời gian bảo hành nếu có lỗi của nhà sản xuất.”
                        Chính sách bảo hành</em>
                </div>
                <div class="col-md-6">
                    <div class="gg-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.4672389120324!2d105.8377614143085!3d21.05399279229366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abad634aac17%3A0x66cbaa9578df29c7!2zQW4gRMawxqFuZywgWcOqbiBQaOG7pSwgVMOieSBI4buTLCBIw6AgTuG7mWksIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1592499052882!5m2!1sen!2s" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div> 
                </div>
            </div>
            <div class="row row_three">
               <div class="col-md-12">
                <div class="left">
                    <span>Copyright © 2020 - Rever. All Rights Reserved.</span>
                </div>
                <div class="right">
                    <a href="">Chính sách bảo mật</a>
                    <a href="">Điều khoản sử dụng</a>
                    <a href="">Phản hồi</a>
                </div>
               </div>
            </div>
        </div>
    </footer>