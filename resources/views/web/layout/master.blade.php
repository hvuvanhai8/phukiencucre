<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Phụ kiện - Ổ cứng SSD, M2-SATA, M2-PCIe, Thẻ nhớ, Phụ kiện</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/web/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/web/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://unpkg.com/smartphoto@1.1.0/css/smartphoto.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">

    <link rel="stylesheet" type="text/css" href="/web/css/style_all.css{{env('APP_ENV')=='local'? '?v='.time():'' }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174300124-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-174300124-1');
    </script>

</head>
<body>

    @include('web.layout.header')



    @yield('content')

    @include('web.layout.footer')



    <script type="text/javascript" src="/web/js/jquery.min.js"></script>
	<script type="text/javascript" src="/web/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/web/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="https://unpkg.com/smartphoto@1.1.0/js/smartphoto.min.js"></script>
    <script type="text/javascript" src="/web/js/popper.min.js"></script>
    <script type="text/javascript" src="/web/js/index.js"></script>

    <script>
        $(document).ready(function(){
            $('#back-top').click(function() {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            });

        });

    </script>

    @yield('script')
</body>
</html>
