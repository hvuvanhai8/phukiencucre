@php
    //dd($category);
@endphp

<div class="nav-menu">
    <div class="menu_home">
        <ul>
            @foreach($category as $key => $cat)
            <li>
                <a href="/{{$cat->cat_slug}}.{{$cat->id}}">
                    @if($cat->id == 1)
                        <i class="fas fa-inbox"></i>
                    @elseif($cat->id == 8)
                        <i class="fas fa-memory"></i>
                    @elseif($cat->id == 14)
                        <i class="fas fa-desktop"></i>
                    @elseif($cat->id == 19)
                        <i class="far fa-keyboard"></i>
                    @endif
                {{$cat->cat_name}}<i class="fal fa-angle-down"></i></a>
                <ul class="menu_child">
                    @foreach($cattegpry_child as $key => $cat_c)
                        @if($cat->id == $cat_c->cat_parent_id)
                        <li><a href="/{{$cat_c->cat_slug}}.{{$cat_c->id}}">{{$cat_c->cat_name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="note"><span>(Mở cửa: 9h - 18h30)</span></div>
</div>

<div id="nav-menu-mb">
    <ul class="navbar_menu">
        @foreach($category as $key => $cat)
            <li>
                <a href="/{{$cat->cat_slug}}.{{$cat->id}}">
                    @if($cat->id == 1)
                        <i class="fas fa-inbox"></i>
                    @elseif($cat->id == 8)
                        <i class="fas fa-memory"></i>
                    @elseif($cat->id == 14)
                        <i class="fas fa-desktop"></i>
                    @elseif($cat->id == 19)
                        <i class="far fa-keyboard"></i>
                    @endif
                    {{$cat->cat_name}}<i class="fal fa-angle-down"></i></a>
                <ul class="menu_child">
                    @foreach($cattegpry_child as $key => $cat_c)
                        @if($cat->id == $cat_c->cat_parent_id)
                            <li><a href="/{{$cat_c->cat_slug}}.{{$cat_c->id}}">{{$cat_c->cat_name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</div>
