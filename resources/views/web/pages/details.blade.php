@php
    //dd($pro_detail);
    $img = explode("|",$pro_detail->pro_image);
    $description = preg_replace("/<table[^>]+\>/i", "(image) ", $pro_detail->pro_description);
   // dd($img);
@endphp

@extends('web.layout.master')

@section('content')
    <div class="banner_top">
    </div>

    <section class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="breackcums">
                    <ol class="breadcrumb">
                        <li><a href="/">Trang chủ <span> &gt; </span></a></li>
                        <li><a href="/{{$cat_parent->cat_slug}}.{{$cat_parent->id}}">{{$cat_parent->cat_name}} <span> &gt; </span></a></li>
                        <li><a href="/{{$pro_detail->pro_category['cat_slug']}}.{{$pro_detail->pro_category['id']}}">{{$pro_detail->pro_category['cat_name']}} <span> &gt; </span></a></li>
                        <li class="active">{{$pro_detail->pro_name}}</li>
                    </ol>
                </div>
            </div>
            <div class="col-md-12">
                <div class="title">
                    <h1>{{$pro_detail->pro_name}}</h1>
                </div>
            </div>
            <div class="col-md-4">
                <div class="slides_new">
                    <div class="main">
                        <div class="slider slider-for">
                            @foreach($img as $key => $item)
                            <div class="item item-for">
                                <a a href="/web/images/product/{{$item}}" class="js-smartPhoto" data-caption="{{$pro_detail->pro_name}}" data-id="camel" data-group="animal">
                                    <img src="/web/images/product/{{$item}}">
                                </a>
                            </div>
                            @endforeach
                        </div>
                        <div class="slider slider-nav">
                            @foreach($img as $key => $item)
                            <div class="item item-nav"><img src="/web/images/product/{{$item}}"></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="detail_product_price">
                            @if($pro_detail->pro_price_discount !== null)
                                <strong class="price">{{$pro_detail->pro_price_discount}}</strong>
                                <span class="old_price_text">
                                    <span class="old_price">{{$pro_detail->pro_price}}</span>
                                 </span>
                            @else
                                <strong class="price">{{$pro_detail->pro_price}}</strong>
                            @endif
                        </div>
                        <div class="teaser_detail">
                            {!! $pro_detail->pro_teaser !!}
                        </div>
                        <a href="/cart.{{$pro_detail->id}}">
                            <button aria-label="Mua ngay" id="cart_button" class="form_button_3 detail_product_button">
                                <i class="fas fa-shopping-cart"></i>Mua ngay
                            </button>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div class="table_detail">
                            <table class="detail_product_short_teaser table"><tbody><tr><th>Dung lượng</th> <td>120 GB</td></tr><tr><th>Đọc/Ghi ngẫu nhiên</th> <td>~ IOPS</td></tr><tr><th>Đọc tuần tự</th> <td>500 MB/s</td></tr><tr><th>Ghi tuần tự</th> <td>~ MB/s</td></tr><tr><th>NAND Flash</th> <td>TLC 3D 32-layer Micron 384Gbit</td></tr></tbody></table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="detail_product_related">
                            <div class="title"><h2>Sản phẩm liên quan</h2></div>
                            <div class="data">
                                @foreach($pro_relate as $key => $pro)
                                @php
                                    $img = explode("|",$pro->pro_image);
                                @endphp
                                <div class="wrapper">
                                    <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}" class="picture"><img src="/web/images/product/{{$img[0]}}" alt=""></a>
                                    <div class="information">
                                    <div class="name"><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">{{$pro->pro_name}}</a></div>
                                        <div class="price">
                                            @if($pro->pro_price_discount !== null)
                                                <strong>{{$pro->pro_price_discount}}</strong>
                                                <span class="old_price">{{$pro->pro_price}}</span>
                                            @else
                                                <strong>{{$pro->pro_price}}</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="detail_description_left">
                    <h2>Giới thiệu sản phẩm SSD 120GB HP S700</h2>
                    <div class="main_description">
                        {!! $description !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h2>Thông số kỹ thuật</h2>
                <div class="table_detail">
                    <table class="detail_product_short_teaser table"><tbody><tr><th>Dung lượng</th> <td>120 GB</td></tr><tr><th>Đọc/Ghi ngẫu nhiên</th> <td>~ IOPS</td></tr><tr><th>Đọc tuần tự</th> <td>500 MB/s</td></tr><tr><th>Ghi tuần tự</th> <td>~ MB/s</td></tr><tr><th>NAND Flash</th> <td>TLC 3D 32-layer Micron 384Gbit</td></tr></tbody></table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            focusOnSelect: true
        });

        window.addEventListener('DOMContentLoaded',function(){
            new SmartPhoto(".js-smartPhoto");
        });
    </script>


@endsection
