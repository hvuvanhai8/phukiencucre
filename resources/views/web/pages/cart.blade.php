@php

@endphp

@extends('web.layout.master')

@section('content')
    <div class="container-fluid">
        <div class="cart_data">
            <div class="cart_note">
                <span class="note-add">Sản phẩm sẽ được giao từ:</span>
                <b>Hà Nội</b>: An Dương, Phường Yên Phụ, Quận Tây Hồ, Hà Nội<br>
            </div>
            @if(!empty($produc_cart))
            <div class="cart_title"><h3>Giỏ hàng của bạn</h3><a class="cart_back text_link_grey" href="/">Mua thêm sản phẩm khác</a></div>
            <table class="cart_table">
                <tbody><tr class="title">
                    <td>Stt.</td>
                    <td>Ảnh</td>
                    <td class="name">Tên sản phẩm</td>
                    <td>Giá bán</td>
                    <td>Số lượng</td>
                    <td>Xóa</td>
                </tr>
                @php $stt = 1; @endphp
                @foreach($produc_cart as $key => $pro)
                @php
                    $img = explode("|",$pro->pro_image);
                @endphp
                <tr class="data">
                    <td class="No">{{$stt++}}.</td>
                    <td class="picture">
                        <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">
                            <img alt="{{$pro->pro_name}}" src="/web/images/product/{{$img[0]}}">
                        </a>
                    </td>
                    <td class="name"><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">{{$pro->pro_name}}</a></td>
                    <td class="price">
                        <strong>{{ $pro->pro_price_discount == null ? $pro->pro_price:$pro->pro_price_discount }}</strong>
                    </td>
                    <td class="quantity">
                        <select class="form_control" onchange="cart({{$pro->id}},{{$pro->price_int}},this.value)">
                            <option value="1" selected="selected">01</option>
                            <option value="2" >02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                        </select>
                    </td>
                    <td class="delete">
                        <a class="text_link" href="javascript:;" onclick="cart('remove', 1702); if($('.cart_table .data').length <= 1) location.reload(); else $(this).closest('tr').remove(); ">
                            <i class="far fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody></table>
            <div class="cart_total">Tổng tiền: <b class="price" id="car_total">{{$total_price}}</b></div>
            <div class="break_module"></div>
            <h3>Thông tin đặt hàng</h3>

            <form class="cart_form" name="cart_form" action="/cart" method="post" onsubmit="return checkForm(this.name, arrCtrlCart);">
                @csrf
                <input type="text" placeholder="Họ và tên *" class="form_control name" name="cart_name" value=""><input type="text" placeholder="Số điện thoại *" class="form_control mobile" name="cart_mobile" value=""><input type="text" placeholder="Địa chỉ nhận hàng" class="form_control information" name="cart_information" value=""><div class="cart_button" style="display: none;"><input type="submit" class="form_button_3" value="Thanh toán khi nhận hàng"></div>
                <div class="cart_button">
                    <a class="form_button_3" href="javascript:;" onclick="$('.cart_form').trigger('submit')">Thanh toán khi nhận hàng
                        <span class="tt-sp">Xem hàng trước - Kiểm tra chuẩn - Thanh toán sau</span>
                    </a>
                </div>
                <div class="cart_hotline">Gọi Hotline <a class="text_link" href="tel:0902131474" rel="nofollow" onclick="send_ga(gaEvents.contact, 'Hotline Other');">0902 131 474</a> để được tư vấn mua hàng.</div>
                <input type="hidden" name="action" value="execute">
            </form>
            @else
            <div class="not_product_cart">
                <i class="fas fa-shopping-cart">
                </i>
            </div>
            <div class="not_cart">Không có sản phẩm nào trong giỏ hàng</div>
            <div class="btn_back">
                <a href="/" title="Trở về  trang chủ">
                    <button class="btn btn-success">Trở về  trang chủ</button>
                </a>
            </div>
            <div class="cart_hotline">Gọi Hotline <a class="text_link" href="tel:0902131474" rel="nofollow" onclick="send_ga(gaEvents.contact, 'Hotline Other');">0902 131 474</a> để được tư vấn mua hàng.</div>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var total_price = {{$total_price}};
        

        function SeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
            }
            return val;
        }

        var total_price_new = SeparateNumber(total_price);
        $('#car_total').html(total_price_new);

        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
            }
            $('#car_total').html(val);
            return;
        }

        function cart(id,price,value){
            console.log('change value');
            console.log('total price', total_price);
            var price_new = price*value;
            total_price = total_price + price_new - price;
            console.log(total_price);
            commaSeparateNumber(total_price)
        }

        /* heloo();

        cart(id,value){
             console.log('id',id);
         }

        var arrCtrlCart= Array("0{#}{#}cart_name{#}Họ và tên","0{#}{#}cart_mobile{#}Số điện thoại");
         function changeCartInfo(){
          $.cookie("cart_name", $(".cart_form .name").val(), { expires: 365, path: "/" });
             $.cookie("cart_mobile", $(".cart_form .mobile").val(), { expires: 365, path: "/" });
           $.cookie("cart_information", $(".cart_form .information").val(), { expires: 365, path: "/" });
        }
        $(function(){ $(".cart_form input[type='text']").keyup(changeCartInfo); });

        */
        
    </script>


@endsection
