@extends('web.layout.master')

@section('content')

    <div class="banner_top">
        <img src="/web/images/banner/banner3.png" alt="" width="100%" height="100%">
    </div>

    <section class="container-fluid">
        <div class="project_hot">
            <div class="row"><div class="col-md-12"><h2 class="title-home">HOT DEAL</h2></div></div>
            <div class="row">
                @foreach($products_hot as $key => $pro)
                    @php
                        $img = explode("|",$pro->pro_image);
                    @endphp
                    <div class="col-md-2 col-sm-4 col-6">
                        <article class="list_article">
                            <div class="cover_image">
                                <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">
                                    @if($pro->pro_price_discount !== null)
                                        <p>SALE {{discount_percent($pro->pro_price_discount,$pro->pro_price)}}</p>
                                    @endif
                                    <img src="/web/images/product/{{$img[0]}}" alt="">
                                    <div class="mark_cover_image">HOT DEAL</div>
                                </a>
                            </div>
                            <div class="info_project">
                                <h3><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}" class="name_project">{{$pro->pro_name}}</a></h3>
                                <div class="price_project">
                                    @if($pro->pro_price_discount !== null)
                                        <span class="curren_price">{{$pro->pro_price_discount}}</span>
                                        <span class="old_price">{{$pro->pro_price}}</span>
                                    @else
                                        <span class="curren_price">{{$pro->pro_price}}</span>
                                    @endif
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="project_hot">
            <div class="row"><div class="col-md-12"><a href="/ram.8" title="Phụ kiện khác"><h2 class="title_home_h">Ram</h2></a></div></div>
            <div class="row">
                @foreach($rams as $key => $pro)
                @php
                    $img = explode("|",$pro->pro_image);
                @endphp
                <div class="col-md-2 col-sm-4 col-6">
                    <article class="list_article">
                        <div class="cover_image">
                            <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">
                                @if($pro->pro_price_discount !== null)
                                    <p>SALE {{discount_percent($pro->pro_price_discount,$pro->pro_price)}}</p>
                                @endif
                                <img src="/web/images/product/{{$img[0]}}" alt="">
                                @if($pro->pro_hot == 1)
                                <div class="mark_cover_image">HOT DEAL</div>
                                @endif
                            </a>
                        </div>
                        <div class="info_project">
                            <h3><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}" class="name_project">{{$pro->pro_name}}</a></h3>
                            <div class="price_project">
                                @if($pro->pro_price_discount !== null)
                                    <span class="curren_price">{{$pro->pro_price_discount}}</span>
                                    <span class="old_price">{{$pro->pro_price}}</span>
                                @else
                                    <span class="curren_price">{{$pro->pro_price}}</span>
                                @endif
                            </div>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
            <div class="row"><div class="col-md-12"><a href="/ram.8" class="more" title="ram">Xem thêm Ram<i class="fal fa-chevron-right"></i></a></div></div>
        </div>

        <div class="project_hot">
            <div class="row"><div class="col-md-12"><a href="/man-hinh.14" title="màn hình"><h2 class="title_home_h">Màn hình</h2></a></div></div>
            <div class="row">
                @foreach($screens as $key => $pro)
                @php
                    $img = explode("|",$pro->pro_image);
                @endphp
                <div class="col-md-2 col-sm-4 col-6">
                    <article class="list_article">
                        <div class="cover_image">
                            <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">
                                @if($pro->pro_price_discount !== null)
                                    <p>SALE {{discount_percent($pro->pro_price_discount,$pro->pro_price)}}</p>
                                @endif
                                <img src="/web/images/product/{{$img[0]}}" alt="">
                                @if($pro->pro_hot == 1)
                                <div class="mark_cover_image">HOT DEAL</div>
                                @endif
                            </a>
                        </div>
                        <div class="info_project">
                            <h3><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}" class="name_project">{{$pro->pro_name}}</a></h3>
                            <div class="price_project">
                                @if($pro->pro_price_discount !== null)
                                    <span class="curren_price">{{$pro->pro_price_discount}}</span>
                                    <span class="old_price">{{$pro->pro_price}}</span>
                                @else
                                    <span class="curren_price">{{$pro->pro_price}}</span>
                                @endif
                            </div>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
            <div class="row"><div class="col-md-12"><a href="/man-hinh.14" class="more" title="Màn hình">Xem thêm màn hình<i class="fal fa-chevron-right"></i></a></div></div>
        </div>

        <div class="project_hot">
            <div class="row"><div class="col-md-12"><a href="/phu-kien-may-tinh.19" title="Phụ kiện khác"><h2 class="title_home_h">Phụ kiện khác</h2></a></div></div>
            <div class="row">
                @foreach($pks as $key => $pro)
                @php
                    $img = explode("|",$pro->pro_image);
                @endphp
                <div class="col-md-2 col-sm-4 col-6">
                    <article class="list_article">
                        <div class="cover_image">
                            <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">
                                @if($pro->pro_price_discount !== null)
                                    <p>SALE {{discount_percent($pro->pro_price_discount,$pro->pro_price)}}</p>
                                @endif
                                <img src="/web/images/product/{{$img[0]}}" alt="">
                                @if($pro->pro_hot == 1)
                                <div class="mark_cover_image">HOT DEAL</div>
                                @endif
                            </a>
                        </div>
                        <div class="info_project">
                            <h3><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}" class="name_project">{{$pro->pro_name}}</a></h3>
                            <div class="price_project">
                                @if($pro->pro_price_discount !== null)
                                    <span class="curren_price">{{$pro->pro_price_discount}}</span>
                                    <span class="old_price">{{$pro->pro_price}}</span>
                                @else
                                    <span class="curren_price">{{$pro->pro_price}}</span>
                                @endif
                            </div>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
            <div class="row"><div class="col-md-12"><a href="/phu-kien-may-tinh.19" class="more" title="Phụ kiện khác">Xem thêm phụ kiện<i class="fal fa-chevron-right"></i></a></div></div>
        </div>
    </section>

@endsection
