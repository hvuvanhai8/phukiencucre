@php
   //dd($category_list);
@endphp

@extends('web.layout.master')

@section('content')

    <div class="banner_top">
    </div>

    <section class="container-fluid">
        <div class="project_hot">
            <div class="row">
                <div class="col-md-12">
                    <div class="breackcums">
                        <ol class="breadcrumb">
                            <li><a href="/">Trang chủ <span> &gt; </span></a></li>
                            @if($cat_parent !== null)
                            <li><a href="/{{$cat_parent->cat_slug}}.{{$cat_parent->id}}">{{$cat_parent->cat_name}} <span> &gt; </span></a></li>
                            @endif
                            <li><a href="/{{$category_list->cat_slug}}.{{$category_list->id}}">{{$category_list->cat_name}}</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row"><div class="col-md-12"><h1 class="title-listing">{{$category_list->cat_name}} ({{count($products)}})</h1></div></div>
            @if(!empty($products->toArray()))
            <div class="row">
                @foreach($products as $key => $pro)
                @php
                    $img = explode("|",$pro->pro_image);
                @endphp
                <div class="col-md-2 col-sm-4 col-6">
                    <article class="list_article">
                        <div class="cover_image">
                            <a href="/{{$pro->pro_slug}}.pk{{$pro->id}}">
                                <p>SALE 10%</p>
                                <img src="/web/images/product/{{$img[0]}}" alt="">
                                <div class="mark_cover_image">HOT DEAL</div>
                            </a>
                        </div>
                        <div class="info_project">
                            <h3><a href="/{{$pro->pro_slug}}.pk{{$pro->id}}" class="name_project">{{$pro->pro_name}}</a></h3>
                            <div class="price_project">
                                @if($pro->pro_price_discount !== null)
                                <span class="curren_price">{{$pro->pro_price_discount}}</span>
                                <span class="old_price">{{$pro->pro_price}}</span>
                                @else
                                    <span class="curren_price">{{$pro->pro_price}}</span>
                                @endif
                            </div>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
            @else
            <div class="row">
                <div class="col-md-12">
                    <p>Không tìm thấy sản phẩm nào !</p>
                </div>
            </div>
            @endif
        </div>
    </section>

@endsection
