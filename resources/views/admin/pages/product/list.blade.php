
@php
    //dd($product->toArray());
@endphp

@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">Danh sách các sản phẩm</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        <strong>{{ session('success') }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách sản phẩm</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th>Danh mục</th>
                                <th>Trạng thái</th>
                                <th>HotDeal</th>
                                <th>Hành động</th>
                            </tr>
                            @php $stt = 1; @endphp
                            @foreach($product as $ket => $pro)
                            @php
                                $img = explode("|",$pro->pro_image);
                            @endphp
                            <tr>
                                <td>{{$stt++}}</td>
                                <td><img src="/web/images/product/{{$img[0]}}" alt="" width="50px" height="50px"></td>
                                <td><a href="/{{$pro->pro_slug.'.pk'.$pro->id}}" target="_blank">{{$pro->pro_name}}</a></td>
                                <td>{{$pro->pro_category['cat_name']}}</td>
                                <td><span class="label label-{{$pro->pro_active == 1?'success':'danger'}}">{{$pro->pro_active == 1?'Active':'NoActive'}}</span></td>
                                <td><span class="label label-{{$pro->pro_hot == 1?'danger':'warning'}}">{{$pro->pro_hot == 1?'Hot':'No Hot'}}</span></td>
                                <td>
                                    <a class="btn btn-warning" href="/admin/products/edit/{{$pro->id}}" title="Sửa"><i class="fa fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-17" title="xóa"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
