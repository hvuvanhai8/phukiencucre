@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">Sản phẩm</li>
            </ol>
        </div>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="row">
            <div class="col-md-4">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Thêm sản phẩm mới</h3>
          </div>
          <div class="box-body">
            <form role="form" enctype="multipart/form-data" action="/admin/products/addProduct" method="POST">
                @csrf
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên sản phẩm</label>
                            <input type="text" class="form-control" name="pro_name" id="" placeholder="Nhập tên sản phẩm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Danh mục sản phẩm</label>
                            <select class="form-control" name="pro_cat_id">
                                @foreach($category as $cat)
                                    <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã sản phẩm</label>
                            <input type="text" class="form-control" name="pro_code" id="" placeholder="Nhập mã sản phẩm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kiểu model</label>
                            <input type="text" class="form-control" name="pro_model" id="" placeholder="Nhập kiểu sản phẩm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng sản phẩm</label>
                            <input type="text" class="form-control" name="pro_quantity" id="" placeholder="Nhập số  lượng sản phẩm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nguồn gốc sản phẩm</label>
                            <input type="text" class="form-control" name="pro_origin" id="" placeholder="Nhập nguồn gốc">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Dung lượng lưu trữ</label>
                            <input type="text" class="form-control" name="pro_capacity" id="" placeholder="Nhập nguồn gốc">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tốc độ đọc ngẫu nhiên</label>
                            <input type="text" class="form-control" name="pro_read_random" id="" placeholder="Nhập tốc độ ngẫu nhiên">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tốc độ đọc</label>
                            <input type="text" class="form-control" name="pro_read" id="" placeholder="Nhập tốc độ đọc">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tốc độ ghi</label>
                            <input type="text" class="form-control" name="pro_write" id="" placeholder="Nhập tốc độ ghi">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Thương hiệu</label>
                            <input type="text" class="form-control" name="pro_brand" id="" placeholder="Nhập thương hiệu">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Thời gian bảo hành</label>
                            <input type="text" class="form-control" name="pro_guarantee" id="" placeholder="Nhập thời gian bảo hành">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Độ bền/ Ổn định</label>
                            <input type="text" class="form-control" name="pro_stability" id="" placeholder="Nhập độ bền">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Cổng kết nối chuẩn</label>
                            <input type="text" class="form-control" name="pro_standard_connect" id="" placeholder="Nhập cổng kết nối">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Bus</label>
                            <input type="text" class="form-control" name="pro_bus" id="" placeholder="Nhập bus">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kiểu sản phẩm</label>
                            <input type="text" class="form-control" name="pro_type" id="" placeholder="Nhập kiểu sản phẩm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Màu sản phẩm</label>
                            <input type="text" class="form-control" name="pro_color" id="" placeholder="Nhập màu">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Độ nhạy (chuột)</label>
                            <input type="text" class="form-control" name="pro_sensitive" id="" placeholder="Nhập độ nhạy">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Khối lượng</label>
                            <input type="text" class="form-control" name="pro_weight" id="" placeholder="Nhập khối lượng">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tầm nhìn (Màn Hình)</label>
                            <input type="text" class="form-control" name="pro_view" id="" placeholder="Nhập tầm nhìn">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Độ sáng (Màn Hình)</label>
                            <input type="text" class="form-control" name="pro_brightness" id="" placeholder="Nhập độ sáng">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ngôn ngữ</label>
                            <input type="text" class="form-control" name="pro_language" id="" placeholder="Nhập ngôn ngữ">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kích thước</label>
                            <input type="text" class="form-control" name="pro_size" id="" placeholder="Nhập kích thước">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Dây</label>
                            <input type="text" class="form-control" name="pro_cord" id="" placeholder="Nhập độ dài dây">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Công tắc</label>
                            <input type="text" class="form-control" name="pro_switch" id="" placeholder="Nhập công tắc">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Thời lượng Pin</label>
                            <input type="text" class="form-control" name="pro_battery" id="" placeholder="Nhập thời lượng pin">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Công suất</label>
                            <input type="text" class="form-control" name="pro_wattage" id="" placeholder="Nhập công suất">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tốc độ xử lý (CPU)</label>
                            <input type="text" class="form-control" name="pro_process_speed" id="" placeholder="Nhập tốc độ">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lõi/luồng (CPU)</label>
                            <input type="text" class="form-control" name="pro_cpu_core" id="" placeholder="Nhập lõi">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả ngắn sản phẩm</label>
                            <textarea name="pro_teaser"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả</label>
                            <textarea name="pro_description"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Trạng thái</label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="pro_active" checked value="1"> Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hot Deal</label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="pro_hot" checked value="1"> Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputFile">Upload ảnh sản phẩm</label>
                        <input type="file" name="images[]" id="" multiple="multiple">
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá sản phẩm</label>
                            <input type="text" class="form-control" name="pro_price" onkeyup="this.value=FormatNumber(this.value);" placeholder="Nhập giá sản phẩm">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá khuyến mãi</label>
                            <input type="text" class="form-control" name="pro_price_discount" onkeyup="this.value=FormatNumber(this.value);" placeholder="Nhập giá khuyễn mãi">
                        </div>
                    </div>
                </div>
                <div class="box-footer" style="margin-left: 20px">
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
            </form>
          </div>

          <!-- /.box-body -->


          <!-- /.box-footer-->

        </div>

        <!-- /.box -->

      </section>
@endsection

@section('script')

<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'pro_description' );
    CKEDITOR.replace( 'pro_teaser' );

    var inputnumber = 'Giá trị nhập vào không phải là số';
    function FormatNumber(str) {
        var strTemp = GetNumber(str);
        if (strTemp.length <= 3)
            return strTemp;
        strResult = "";
        for (var i = 0; i < strTemp.length; i++)
            strTemp = strTemp.replace(",", "");
        var m = strTemp.lastIndexOf(".");
        if (m == -1) {
            for (var i = strTemp.length; i >= 0; i--) {
                if (strResult.length > 0 && (strTemp.length - i - 1) % 3 == 0)
                    strResult = "," + strResult;
                strResult = strTemp.substring(i, i + 1) + strResult;
            }
        } else {
            var strphannguyen = strTemp.substring(0, strTemp.lastIndexOf("."));
            var strphanthapphan = strTemp.substring(strTemp.lastIndexOf("."),
                strTemp.length);
            var tam = 0;
            for (var i = strphannguyen.length; i >= 0; i--) {

                if (strResult.length > 0 && tam == 4) {
                    strResult = "," + strResult;
                    tam = 1;
                }

                strResult = strphannguyen.substring(i, i + 1) + strResult;
                tam = tam + 1;
            }
            strResult = strResult + strphanthapphan;
        }
        return strResult;
    }

    function GetNumber(str) {
        var count = 0;
        for (var i = 0; i < str.length; i++) {
            var temp = str.substring(i, i + 1);
            if (!(temp == "," || temp == "." || (temp >= 0 && temp <= 9))) {
                alert(inputnumber);
                return str.substring(0, i);
            }
            if (temp == " ")
                return str.substring(0, i);
            if (temp == ".") {
                if (count > 0)
                    return str.substring(0, ipubl_date);
                count++;
            }
        }
        return str;
    }

    function IsNumberInt(str) {
        for (var i = 0; i < str.length; i++) {
            var temp = str.substring(i, i + 1);
            if (!(temp == "." || (temp >= 0 && temp <= 9))) {
                alert(inputnumber);
                return str.substring(0, i);
            }
            if (temp == ",") {
                return str.substring(0, i);
            }
        }
        return str;
    }
</script>

@endsection
