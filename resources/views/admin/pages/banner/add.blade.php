@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <div><ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
                <li class="active">Banner</li>
            </ol></div>
        <h1>
            Banner
            <small>Add</small>
        </h1>
    </section>
    <section class="content">

      

        <!-- Default box -->
  
        <div class="box">
  
          <div class="box-header with-border">
  
            <h3 class="box-title">Thêm ảnh mới</h3>
  
          </div>
  
        
  
          <div class="box-body">
            <form role="form" enctype="multipart/form-data" action="/admin/banner/addimage" method="POST">
                @csrf
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Banner Name</label>
                            <input type="text" class="form-control" name="ban_name" id="" placeholder="Enter banner name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Trạng thái</label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="ban_active" checked> Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputFile">Photo</label>
                        <input type="file" name="images[]" id="" multiple="multiple">
                    </div>
                </div>
                <div class="box-footer" style="margin-left: 20px">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
          </div>
  
          <!-- /.box-body -->

  
          <!-- /.box-footer-->
  
        </div>
  
        <!-- /.box -->
  
      </section>
@endsection