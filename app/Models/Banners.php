<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    //
    protected $table = 'banners';

    protected $fillable = [
        'ban_name',
        'ban_link',
        'ban_active',
        'ban_picture',
    ];
}
