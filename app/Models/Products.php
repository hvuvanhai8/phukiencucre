<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = 'products';

    protected $fillable = [
        'pro_code',
        'pro_model',
        'pro_name',
        'pro_slug',
        'pro_quantity',
        'pro_image',
        'pro_origin',
        'pro_capacity',
        'pro_read_random',
        'pro_read',
        'pro_write',
        'pro_brand',
        'pro_guarantee',
        'pro_stability',
        'pro_standard_connect',
        'pro_bus',
        'pro_type',
        'pro_color',
        'pro_sensitive',
        'pro_weight',
        'pro_view',
        'pro_brightness',
        'pro_language',
        'pro_size',
        'pro_cord',
        'pro_switch',
        'pro_battery',
        'pro_wattage',
        'pro_process_speed',
        'pro_cpu_core',
        'pro_teaser',
        'pro_description',
        'pro_price',
        'pro_price_discount',
        'pro_active',
        'pro_rating',
    ];

    public function pro_category()
    {
        return $this->belongsTo(Category::class, 'pro_cat_id', 'id');
    }
}
