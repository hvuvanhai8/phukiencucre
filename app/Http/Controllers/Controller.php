<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Products;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // share biến cate ra tất cả các view
        $category = Category::select('id','cat_name','cat_parent_id','cat_slug','cat_type','cat_rewrite')
            ->where('cat_parent_id',"=",0)->get();
        $cattegpry_child = Category::select('id','cat_name','cat_parent_id','cat_slug','cat_type','cat_rewrite')
            ->where('cat_parent_id',"!=",0)->get();

        if(isset($_COOKIE['id_product'])){
            $cookie = $_COOKIE['id_product'];
            $list_id = explode(",", $cookie);
            $produc_cart = Products::select('id','pro_name','pro_slug','pro_name','pro_image','pro_price','pro_price_discount')
                ->whereIn('id',$list_id)
                ->get();
            $count_cart = count($produc_cart);
        }else{
            $count_cart = 0;
        }

        View::share('category', $category);
        View::share('cattegpry_child', $cattegpry_child);
        View::share('count_cart', $count_cart);
    }

    // function getParentCategory(){
    //     $category = Category::all();
    //     $cat_parent = $category->groupBy('cat_parent_id');
    //     if (isset($cat_parent[0])){
    //         foreach ($cat_parent[0] as $key=>$cat_item){
    //             $cat_parent[0][$key]->cat_child = isset($cat_parent[$cat_item->cat_id])?$cat_parent[$cat_item->cat_id]:[];
    //         }
    //     }
    //     return $cat_parent;
    // }
}
