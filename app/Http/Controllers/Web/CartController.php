<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Products;

class CartController extends Controller
{
    //
    public function getCart($id=0){
        if(isset($_COOKIE['id_product'])){
            if($id !==0){
                setcookie("id_product", $id.','.$_COOKIE['id_product'], time()+3600, "/", "",  0);
            }
            
            $cookie = $_COOKIE['id_product'];
            $list_id = explode(",", $cookie);
            $list_id[] = $id;
            $produc_cart = Products::select('id','pro_name','pro_slug','pro_name','pro_image','pro_price','pro_price_discount')
                ->whereIn('id',$list_id)
                ->get();
        }else{
            if($id !==0){
                setcookie("id_product", $id, time()+3600, "/", "",  0);
            }
            $produc_cart = Products::select('id','pro_name','pro_slug','pro_name','pro_image','pro_price','pro_price_discount')
            ->where('id',$id)
            ->get();
        }
        // return view('web.pages.cart',[
        //     'produc_cart' => $produc_cart
        // ]);
        
        return redirect('/cart');
    }
    public function getCartAll(){
        if(isset($_COOKIE['id_product'])){
            $cookie = $_COOKIE['id_product'];
            $list_id = explode(",", $cookie);
            $produc_cart = Products::select('id','pro_name','pro_slug','pro_name','pro_image','pro_price','pro_price_discount')
                ->whereIn('id',$list_id)
                ->get();
        }else{
            $produc_cart = [];
        }
        // dd($produc_cart);
        $total_price = null;
        foreach($produc_cart as $key => $pro){
            $b = $pro->pro_price_discount === null ? $pro->pro_price:$pro->pro_price_discount;
            $a = str_replace(',','',$b);
            $a = (int) $a;
            $total_price += $a;
            $pro->price_int = $a;
        }
        // $total_price = number_format($total_price);
        // dd($produc_cart);
        return view('web.pages.cart',[
            'produc_cart' => $produc_cart,
            'total_price' => $total_price
        ]);
    }
}
