<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Models\Category;
use App\Models\Products;

class HomeController extends Controller
{
    //
    public function getTest(){
        return view('welcome');
    }

    public function getHome(){
        $products_hot = Products::select('id','pro_hot','pro_slug','pro_cat_id','pro_name','pro_image','pro_active','pro_price','pro_price_discount')
            ->where('pro_active','=',1)
            ->where('pro_hot','=',1)
            ->limit(12)
            ->get();
//        dd($products_hot);
        $cat_rams = Category::find(8);
        $array_cat_rams = explode(',',$cat_rams->cat_all_child);
        $rams = Products::select('id','pro_hot','pro_slug','pro_cat_id','pro_name','pro_image','pro_active','pro_price','pro_price_discount')
        ->wherein('pro_cat_id',$array_cat_rams)
        ->where('pro_active','=',1)
        ->limit(6)
        ->get();

        // $cat
        $cat_screen = Category::find(14);
        $array_cat_screen = explode(',',$cat_screen->cat_all_child);
        $screens = Products::select('id','pro_hot','pro_slug','pro_cat_id','pro_name','pro_image','pro_active','pro_price','pro_price_discount')
        ->wherein('pro_cat_id',$array_cat_screen)
        ->where('pro_active','=',1)
        ->limit(6)
        ->get();

        $cat_pk = Category::find(19);
        $array_cat_pk = explode(',',$cat_pk->cat_all_child);
        $pks = Products::select('id','pro_hot','pro_slug','pro_cat_id','pro_name','pro_image','pro_active','pro_price','pro_price_discount')
        ->wherein('pro_cat_id',$array_cat_pk)
        ->where('pro_active','=',1)
        ->limit(6)
        ->get();

        // dd($pks);
      


        return view('web.pages.home',[
            'products_hot' => $products_hot,
            'rams' => $rams,
            'screens' => $screens,
            'pks' => $pks,
            'cat_rams' => $cat_rams,
            'cat_screen' => $cat_screen,
            'cat_pk' => $cat_pk
        ]);
    }
}
