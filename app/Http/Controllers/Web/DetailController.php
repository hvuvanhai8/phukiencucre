<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Models\Products;
use App\Models\Category;

class DetailController extends Controller
{
    //
    public function getDetail($pro_slug,$id){

        
        

        $pro_detail = Products::select('*')
        ->with(['pro_category:id,cat_name,cat_rewrite,cat_slug'])
        ->where('id','=',$id)
        ->first();

      
        $category_list = Category::find($pro_detail->pro_cat_id);
        $cat_parent_id = $category_list->cat_parent_id;
        $cat_parent = Category::find($cat_parent_id);

        
       
        $array_cat_child = $cat_parent->cat_all_child;
        $array_cat_child = explode(',',$array_cat_child);
       
        // dd($array_cat_child);
        $pro_relate = Products::select('id','pro_slug','pro_name','pro_price','pro_price_discount','pro_active','pro_image')
                    ->where('pro_active','=',1)
                    ->wherein('pro_cat_id',$array_cat_child)
                    ->orderBy('id','DESC')
                    ->get();
                    
        // dd($pro_relate);
        // if($cat_parent !== null){
        //     $pro_relate = $pro_relate->wherein('pro_cat_id',)
        // }else{
            
        
        return view('web.pages.details',[
            "pro_detail" => $pro_detail,
            "cat_parent" => $cat_parent,
            "pro_relate" => $pro_relate
        ]);
    }
}
