<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Products;

class ListingController extends Controller
{
    //
    public function getListing($slug,$id){

        $category_list = Category::find($id);
        $cat_parent_id = $category_list->cat_parent_id;
        $array_cat = [];
        $cat_parent = null;
        if($cat_parent_id == 0){
            $array_cat = $category_list->cat_all_child;
            $array_cat = explode(',',$array_cat);
        }else{
            $array_cat[] = $category_list->id;
            $cat_parent = Category::find($cat_parent_id);
        };

        $products = Products::select('id','pro_hot','pro_slug','pro_cat_id','pro_name','pro_image','pro_active','pro_price','pro_price_discount')
            ->wherein('pro_cat_id',$array_cat)
            ->where('pro_active','=',1)
            ->get();

    //    dd($cat_parent);

        return view('web.pages.list',[
            'products' => $products,
            'category_list' => $category_list,
            'cat_parent' => $cat_parent
        ]);
    }
}
