<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banners;
use Validator;

class BannersController extends Controller
{
    //
    public function list(){
        return view('admin.pages.banner.list');
    }

    public function add(){
        // dd(123);
        return view('admin.pages.banner.add');
    }

    public function addImage(Request $request){

        // if ($request->hasFile('ban_picture')) {
        //     $picture = $request->file('ban_picture');
        //     $name = time().'.'.$picture->getClientOriginalExtension();
        //     $destinationPath = public_path('/web/images/test');
        //     $picture->move($destinationPath, $name);
        //     $banner->ban_picture = $name;
        // }

        // $banner->ban_picture = implode("|",$images);

        $banner = new Banners();

        $input=$request->all();
        $images=[];
        if($files=$request->file('images')){
            foreach($files as $file){
                $name=time().'.'.$file->getClientOriginalName();
                $destinationPath = public_path('/web/images/banner');
                $file->move($destinationPath,$name);
                $images[]=$name;
            }
        }
        $banner->ban_picture = implode("|",$images);
        if($banner->save()){
            return view('admin.pages.banner.list');
        }
    }
}
