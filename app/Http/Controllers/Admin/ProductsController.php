<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Validator;
use App\Models\Products;
use App\Models\Category;

class ProductsController extends Controller
{
    //
    public function list(){
        $product = Products::select('id','pro_slug','pro_hot','pro_cat_id','pro_name','pro_image','pro_active')
            ->with(['pro_category:id,cat_name,cat_rewrite'])
            ->get();
//        dd($product->toArray());
        return view('admin.pages.product.list',[
            'product'=>$product
        ]);
    }
    public function add(){
        $cat = Category::select('id','cat_name','cat_active')
            ->where('cat_active',1)
            ->whereNotIn('cat_parent_id',[0])
            ->get();
        return view('admin.pages.product.add',[
            "category" => $cat,
        ]);
    }
    public function addProduct(Request $request){
        $validate = Validator::make(
            $request->all(),
            [
                'pro_name' => 'required|min:5|max:255',
            ],

            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],

            [
                'pro_name' => 'Tên sản phẩm',
                'pro_image' => 'Ảnh sản phẩm'
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }else{
            $product = new Products;

            $product->pro_cat_id = $request->pro_cat_id;
            $product->pro_code = $request->pro_code;
            $product->pro_model = $request->pro_model;
            $product->pro_name = $request->pro_name;
            $product->pro_slug = Str::slug($request->pro_name,'-');
            $product->pro_quantity = $request->pro_quantity;
            $product->pro_origin = $request->pro_origin;
            $product->pro_capacity = $request->pro_capacity;
            $product->pro_read_random = $request->pro_read_random;
            $product->pro_read = $request->pro_read;
            $product->pro_write = $request->pro_write;
            $product->pro_brand = $request->pro_brand;
            $product->pro_guarantee = $request->pro_guarantee;
            $product->pro_stability = $request->pro_stability;
            $product->pro_standard_connect = $request->pro_standard_connect;
            $product->pro_bus = $request->pro_bus;
            $product->pro_type = $request->pro_type;
            $product->pro_color = $request->pro_color;
            $product->pro_sensitive = $request->pro_sensitive;
            $product->pro_weight = $request->pro_weight;
            $product->pro_view = $request->pro_view;
            $product->pro_brightness = $request->pro_brightness;
            $product->pro_language = $request->pro_language;
            $product->pro_size = $request->pro_size;
            $product->pro_cord = $request->pro_cord;
            $product->pro_switch = $request->pro_switch;
            $product->pro_battery = $request->pro_battery;
            $product->pro_wattage = $request->pro_wattage;
            $product->pro_process_speed = $request->pro_process_speed;
            $product->pro_cpu_core = $request->pro_cpu_core;
            $product->pro_teaser = $request->pro_teaser;
            $product->pro_description = $request->pro_description;
            $product->pro_price = $request->pro_price;
            $product->pro_price_discount = $request->pro_price_discount;
            $product->pro_active = $request->pro_active;
            $product->pro_rating = $request->pro_rating;
            $product->pro_hot = $request->pro_hot;
            $product->pro_price = $request->pro_price;
            $product->pro_price_discount = $request->pro_price_discount;

            $images=[];
            if($files=$request->file('images')){
                foreach($files as $file){
                    $name=time().'.'.$file->getClientOriginalName();
                    $destinationPath = public_path('/web/images/product');
                    $file->move($destinationPath,$name);
                    $images[]=$name;
                }
            }
            $product->pro_image = implode("|",$images);

            if($product->save()){
                return redirect('/admin/products/list')->with(['success'=>' Thêm mới sản phẩm thành công !']);
            }
        }
    }

    public function edit($id){
        $product = Products::findOrFail($id);
        $cat = Category::select('id','cat_name','cat_active')
            ->where('cat_active',1)
            ->whereNotIn('cat_parent_id',[0])
            ->get();

        return view('admin.pages.product.edit',[
            "product" => $product,
            "category" => $cat
        ]);
    }

    public function editProduct(Request $request,$id){
        $validate = Validator::make(
            $request->all(),
            [
                'pro_name' => 'required|min:5|max:255',
            ],

            [
                'required' => ':attribute Không được để trống',
                'min' => ':attribute Không được nhỏ hơn :min',
                'max' => ':attribute Không được lớn hơn :max',
                'unique' => ':attribute Không được trùng lặp'
            ],

            [
                'pro_name' => 'Tên sản phẩm',
                'pro_image' => 'Ảnh sản phẩm'
            ]
        );

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }else{
            $product = Products::findOrFail($id);

            $product->pro_cat_id = $request->pro_cat_id;
            $product->pro_code = $request->pro_code;
            $product->pro_model = $request->pro_model;
            $product->pro_name = $request->pro_name;
            $product->pro_slug = Str::slug($request->pro_name,'-');
            $product->pro_quantity = $request->pro_quantity;
            $product->pro_origin = $request->pro_origin;
            $product->pro_capacity = $request->pro_capacity;
            $product->pro_read_random = $request->pro_read_random;
            $product->pro_read = $request->pro_read;
            $product->pro_write = $request->pro_write;
            $product->pro_brand = $request->pro_brand;
            $product->pro_guarantee = $request->pro_guarantee;
            $product->pro_stability = $request->pro_stability;
            $product->pro_standard_connect = $request->pro_standard_connect;
            $product->pro_bus = $request->pro_bus;
            $product->pro_type = $request->pro_type;
            $product->pro_color = $request->pro_color;
            $product->pro_sensitive = $request->pro_sensitive;
            $product->pro_weight = $request->pro_weight;
            $product->pro_view = $request->pro_view;
            $product->pro_brightness = $request->pro_brightness;
            $product->pro_language = $request->pro_language;
            $product->pro_size = $request->pro_size;
            $product->pro_cord = $request->pro_cord;
            $product->pro_switch = $request->pro_switch;
            $product->pro_battery = $request->pro_battery;
            $product->pro_wattage = $request->pro_wattage;
            $product->pro_process_speed = $request->pro_process_speed;
            $product->pro_cpu_core = $request->pro_cpu_core;
            $product->pro_teaser = $request->pro_teaser;
            $product->pro_description = $request->pro_description;
            $product->pro_price = $request->pro_price;
            $product->pro_price_discount = $request->pro_price_discount;
            $product->pro_active = $request->pro_active;
            $product->pro_rating = $request->pro_rating;
            $product->pro_hot = $request->pro_hot;
            $product->pro_price = $request->pro_price;
            $product->pro_price_discount = $request->pro_price_discount;

            $images=[];
            if($files=$request->file('images')){
                foreach($files as $file){
                    $name=time().'.'.$file->getClientOriginalName();
                    $destinationPath = public_path('/web/images/product');
                    $file->move($destinationPath,$name);
                    $images[]=$name;
                }
                $product->pro_image = implode("|",$images);
            }

            if($product->save()){
                return redirect('/admin/products/list')->with(['success'=>'Sửa sản phẩm thành công !']);
            }
        }
    }
}
