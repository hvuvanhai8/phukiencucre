const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js');
// mix.sass('resources/sass/app.scss', 'public/css');

mix.sass('resources/sass/scss_admin/style_all.scss', 'public/admin/style');

// js frontend
mix.js('resources/js/js_all.js', 'public/web/js');

// css frontend
mix.sass('resources/sass/scss_web/style_all.scss', 'public/web/css');